/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package frc.team2856;

import edu.wpi.first.wpilibj.Jaguar;

/**
 *
 * @author Jan
 */
public class LimitedJaguar extends Jaguar{

    private double accel, target;
    private boolean manual = false;
    
    public LimitedJaguar(int slot, int channel, double a) {
	super(slot, channel);
	accel = a;
	target = 0;
    }
    
    public LimitedJaguar(int channel, double a) {
	this(1, channel, a);
    }
    
    public void set(double s) {
	if(manual)
            super.set(s);
        else
            target = s;
    }
    
    public void setManual(boolean man) {
        manual = man;
    }
    
    public boolean getManual() {
       return manual; 
    }
    
    public double getTarget() {
	return target;
    }
    
    public void setAccel(double a) {
	accel = a;
    }
    
    public double getAccel() {
	return accel;
    }
    
    public void update() {
	if(manual) { return; }
	
        double curr = get();
	if(target > curr) 
	    super.set(curr + Math.min(accel, target-curr));
        else
            super.set(curr - Math.min(accel, curr-target));
    }
    
    public void stop() {
	super.set(0);
    }
    
}