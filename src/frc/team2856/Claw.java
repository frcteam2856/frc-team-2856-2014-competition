/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc.team2856;

import edu.wpi.first.wpilibj.AnalogChannel;
import edu.wpi.first.wpilibj.DriverStationLCD;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.SpeedController;

/**
 *
 * @author jeschneider
 */
public class Claw {

    private static final double DEFAULT_MOTOR_SPEED = 0.2;
    private static final int MAX_VALUE = 255;
    private static final double L_MID_VOLTAGE = 2.42, L_RNGE_VOLTAGE = 1.58;
    private static final double L_PIV_MID_VOLTAGE = 3.7, L_PIV_RNGE_VOLTAGE = 0.3;
    //private static final double R_MID_VOLTAGE, R_RNGE_VOLTAGE;
    private static final double R_PIV_MID_VOLTAGE = 3.25, R_PIV_RNGE_VOLTAGE = 0.25;
    private static final int L_MAX_VALUE = 255;
    private static final int L_MIN_VALUE = 0;
    private static final int LPIV_MAX_VALUE = 255;
    private static final int LPIV_MIN_VALUE = 0;
    private static final int R_MAX_VALUE = 255;
    private static final int R_MIN_VALUE = 0;
    private static final int RPIV_MAX_VALUE = 255;
    private static final int RPIV_MIN_VALUE = 0;
    
    private SpeedController leftMotor, rightMotor, leftPivotMotor, rightPivotMotor;
    private AnalogChannel leftLP, rightLP, leftPivotLP, rightPivotLP;

    private int leftTarg, leftPivTarg, rightTarg, rightPivTarg;
    boolean manual = true, targChanged = false, enforceLimits = true;
    
    private DriverStationLCD dslcd;
    
    public Claw() {
	leftMotor = new Victor(RobotMap.CLAW_MOTOR_SLOT,
		RobotMap.LEFT_CLAW_MOTOR_CHANNEL);
	rightMotor = new Victor(RobotMap.CLAW_MOTOR_SLOT,
		RobotMap.RIGHT_CLAW_MOTOR_CHANNEL);
	leftPivotMotor = new Victor(RobotMap.CLAW_MOTOR_SLOT,
		RobotMap.LEFT_CLAW_PIVOT_CHANNEL);
	rightPivotMotor = new Victor(RobotMap.CLAW_MOTOR_SLOT,
		RobotMap.RIGHT_CLAW_PIVOT_CHANNEL);
	leftLP = new AnalogChannel(RobotMap.CLAW_LP_SLOT,
		RobotMap.LEFT_CLAW_LP_CHANNEL);
	rightLP = new AnalogChannel(RobotMap.CLAW_LP_SLOT,
		RobotMap.RIGHT_CLAW_LP_CHANNEL);
	leftPivotLP = new AnalogChannel(RobotMap.CLAW_LP_SLOT,
		RobotMap.LEFT_CLAW_PIVOT_LP_CHANNEL);
	rightPivotLP = new AnalogChannel(RobotMap.CLAW_LP_SLOT,
		RobotMap.RIGHT_CLAW_PIVOT_LP_CHANNEL);
	
	leftTarg = 0;
	leftPivTarg = 0;
	rightTarg = 0;
	rightPivTarg = 0;
        
        dslcd = DriverStationLCD.getInstance();
    }
    
    public void setLeftTarget(int claw, int pivot) {
	if(claw < L_MIN_VALUE || claw > L_MAX_VALUE
		|| pivot < LPIV_MIN_VALUE || pivot > LPIV_MAX_VALUE) {
	    throw new IllegalArgumentException(
		    "claw and/or pivot value out of bounds");
	}
	manual = false;
	targChanged = true;
	leftTarg = claw;
	leftPivTarg = pivot;
    }
    
    public void setLeftClawTarget(int claw) {
	if(claw < L_MIN_VALUE || claw > L_MAX_VALUE) {
	    throw new IllegalArgumentException(
		    "claw value out of bounds");
	}
	manual = false;
	targChanged = true;
	leftTarg = claw;
    }
    
    public void setLeftPivotTarget(int pivot) {
	if(pivot < LPIV_MIN_VALUE || pivot > LPIV_MAX_VALUE) {
	    throw new IllegalArgumentException(
		    "pivot value out of bounds");
	}
	manual = false;
	targChanged = true;
	leftPivTarg = pivot;
    }
    
    public void setRightTarget(int claw, int pivot) {
	if(claw < R_MIN_VALUE || claw > R_MAX_VALUE
		|| pivot < RPIV_MIN_VALUE || pivot > RPIV_MAX_VALUE) {
	    throw new IllegalArgumentException(
		    "claw and/or pivot value out of bounds");
	}
	manual = false;
	targChanged = true;
	rightTarg = claw;
	rightPivTarg = pivot;
    }
    
    public void setRightClawTarget(int claw) {
        if(claw < R_MIN_VALUE || claw > R_MAX_VALUE) {
	    throw new IllegalArgumentException(
		    "claw value out of bounds");
	}
	manual = false;
	targChanged = true;
	rightTarg = claw;
    }
    
    public void setRightPivotTarget(int pivot) {
        if(pivot < RPIV_MIN_VALUE || pivot > RPIV_MAX_VALUE) {
	    throw new IllegalArgumentException(
		    "pivot value out of bounds");
	}
	manual = false;
	targChanged = true;
	rightPivTarg = pivot;
    }
    
    public void moveRight(double speed) { //horiz
	manual = true;
        //if(enforceLimits)
       //   if( (rightLP.getVoltage() < (R_MID_VOLTAGE - R_RNGE_VOLTAGE) && rightMotor.get() < 0) || (rightLP.getVoltage() > R_MID_VOLTAGE + R_RNGE_VOLTAGE && leftMotor.get() > 0) )
         //     rightMotor.set(0);
        //System.out.println("moveRight raw: "+speed);
        if(speed > 0)//right
            speed = Math.min(speed, RobotMap.CLAW_ARM_MAX_EFFORT);
        else//left
            speed = Math.max(speed, -RobotMap.CLAW_ARM_MAX_EFFORT);
        //System.out.println("moveRight: "+speed);
	rightMotor.set(speed);
    }
    
    public void pivotRight(double speed) { //vert
	manual = true;
        System.out.println("Right Pivot Voltage: "+rightPivotLP.getVoltage());
        System.out.println("Right Pivot MID Voltage: "+R_PIV_MID_VOLTAGE);
        System.out.println("Right Pivot RNGE Voltage: "+R_PIV_RNGE_VOLTAGE);
        System.out.println("Right Pivot Speed: "+rightPivotMotor.get());
        if(enforceLimits)
            if( ( (rightPivotLP.getVoltage() < (R_PIV_MID_VOLTAGE - R_PIV_RNGE_VOLTAGE) ) && rightPivotMotor.get() < 0) || ( ( rightPivotLP.getVoltage() > (R_PIV_MID_VOLTAGE + R_PIV_RNGE_VOLTAGE) ) && ( rightPivotMotor.get() > 0 ) ) ) {
                rightPivotMotor.set(0);
                System.out.println("HIT LIMIT HIT LIMIT HIT LIMIT HIT LIMIT HIT LIMIT AHHHHHH!!!!!!!!!!!!1");
                return;
           }
        if(speed > 0) //up
            speed = Math.min(RobotMap.CLAW_PIV_MAX_EFFORT, speed);
        else //down
            speed = .28*Math.max(-RobotMap.CLAW_PIV_MAX_EFFORT, speed);
        System.out.println("pivotRight: "+speed);
	rightPivotMotor.set(speed);
    }
    
    public void moveLeft(double speed) { //horiz
	manual = true;
        if(enforceLimits)
            if( (leftLP.getVoltage() < L_MID_VOLTAGE - L_RNGE_VOLTAGE && leftMotor.get() > 0) || (leftLP.getVoltage() > L_MID_VOLTAGE+L_RNGE_VOLTAGE && leftMotor.get() < 0) )
                leftMotor.set(0);
        System.out.println("Raw Move Left: "+speed);
        if(speed > 0)//right
            speed = Math.min(speed, RobotMap.CLAW_ARM_MAX_EFFORT);
        else//left
            speed = Math.max(speed, -RobotMap.CLAW_ARM_MAX_EFFORT);
        System.out.println("moveLeft: "+speed);
	leftMotor.set(speed);
    }
    
    public void pivotLeft(double speed) { //vert
	manual = true;
        if(enforceLimits)
            if( (leftPivotLP.getVoltage() < L_PIV_MID_VOLTAGE - L_RNGE_VOLTAGE && leftPivotMotor.get() < 0) || (leftPivotLP.getVoltage() > L_PIV_MID_VOLTAGE - L_PIV_RNGE_VOLTAGE && leftPivotMotor.get() > 0) )
                leftPivotMotor.set(0);
        if(speed > 0) //up
            speed = Math.min(RobotMap.CLAW_PIV_MAX_EFFORT, speed);
        else //down
            speed = .19*Math.max(-RobotMap.CLAW_PIV_MAX_EFFORT, speed);
        //System.out.println("pivotLeft: "+speed);
	leftPivotMotor.set(speed);
    }
    
    public void update() {
        if(enforceLimits) {
            if( (leftLP.getVoltage() < L_MID_VOLTAGE - L_RNGE_VOLTAGE && leftMotor.get() > 0) || (leftLP.getVoltage() > L_MID_VOLTAGE+L_RNGE_VOLTAGE && leftMotor.get() < 0) )
                leftMotor.set(0);
            if( (leftPivotLP.getVoltage() < L_PIV_MID_VOLTAGE - L_RNGE_VOLTAGE && leftPivotMotor.get() < 0) || (leftPivotLP.getVoltage() > L_PIV_MID_VOLTAGE - L_PIV_RNGE_VOLTAGE && leftPivotMotor.get() > 0) )
                leftPivotMotor.set(0);
       //   if( (rightLP.getVoltage() < R_MID_VOLTAGE - R_RNGE_VOLTAGE && rightMotor.get() < 0) || (rightLP.getVoltage() > R_MID_VOLTAGE + R_RNGE_VOLTAGE && leftMotor.get() > 0) )
         //     rightMotor.set(0);
            if( (rightPivotLP.getVoltage() < R_PIV_MID_VOLTAGE - R_PIV_RNGE_VOLTAGE && rightPivotMotor.get() < 0) || (rightPivotLP.getVoltage() > R_PIV_MID_VOLTAGE + R_PIV_RNGE_VOLTAGE && rightPivotMotor.get() > 0) )
                rightPivotMotor.set(0);
        }
	/*if(manual) { return; }
	
        int leftPos = (int)((leftLP.getVoltage() / 5) * MAX_VALUE);
	int leftPivPos = (int)((leftPivotLP.getVoltage() / 5) * MAX_VALUE);
	int rightPos = (int)((rightLP.getVoltage() / 5) * MAX_VALUE);
	int rightPivPos = (int)((rightPivotLP.getVoltage() / 5) * MAX_VALUE);
	int leftDir = (leftPos < leftTarg ? 1 : -1);
	int leftPivDir = (leftPivPos < leftPivTarg ? 1 : -1);
	int rightDir = (rightPos < rightTarg ? 1 : -1);
	int rightPivDir = (rightPivPos < rightPivTarg ? 1 : -1);
	
	if(targChanged) {
	    leftMotor.set(DEFAULT_MOTOR_SPEED * leftDir);
	    leftPivotMotor.set(DEFAULT_MOTOR_SPEED * leftPivDir);
	    rightMotor.set(DEFAULT_MOTOR_SPEED * rightDir);
	    rightPivotMotor.set(DEFAULT_MOTOR_SPEED * rightPivDir);
	    targChanged = false;
	}
	
	if(leftMotor.get() * leftDir < 0) {
	    leftMotor.set(DEFAULT_MOTOR_SPEED * leftDir);
	} if(leftPivotMotor.get() * leftPivDir < 0) {
	    leftPivotMotor.set(DEFAULT_MOTOR_SPEED * leftPivDir);
	} if(rightMotor.get() * rightDir < 0) {
	    rightMotor.set(DEFAULT_MOTOR_SPEED * rightDir);
	} if(rightPivotMotor.get() * rightPivDir < 0) {
	    rightPivotMotor.set(DEFAULT_MOTOR_SPEED * rightPivDir);
	}
	
	if(leftPos == leftTarg) {
	    leftMotor.set(0);
	} if(leftPivPos == leftPivTarg) {
	    leftPivotMotor.set(0);
	} if(rightPos == rightTarg) {
	    rightMotor.set(0);
	} if(rightPivPos == rightPivTarg) {
	    rightPivotMotor.set(0);
	}*/
        
        dslcd.println(DriverStationLCD.Line.kUser1, 1, "left: " + leftLP.getVoltage());
        dslcd.println(DriverStationLCD.Line.kUser2, 1, "leftPiv: " + leftPivotLP.getVoltage());
        dslcd.println(DriverStationLCD.Line.kUser3, 1, "right: " + rightLP.getVoltage());
        dslcd.println(DriverStationLCD.Line.kUser4, 1, "rightPiv: " + rightPivotLP.getVoltage());
        dslcd.updateLCD();
    }

    public void stop() {
	manual = true;
	
	leftMotor.set(0);
	leftPivotMotor.set(0);
	rightMotor.set(0);
	rightPivotMotor.set(0);
    }
    
    public double getLeftVoltage() {
	return leftLP.getVoltage();
    }
    
    public double getLeftPivotVoltage() {
	return leftPivotLP.getVoltage();
    }
    
    public double getRightVoltage() {
	return rightLP.getVoltage();
    }
    
    public double getRightPivotVoltage() {
	return rightPivotLP.getVoltage();
    }
    
    public int getLeftTarget() {
        return leftTarg;
    }
    
    public int getRightTarget() {
	return rightTarg;
    }
    
    public int getLeftPivotTarget() {
	return leftPivTarg;
    }
    
    public int getRightPivotTarget() {
        return rightPivTarg;
    }    
}