/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frc.team2856;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;

/**
 *
 * @author mitchell
 */
public class CustomPIDController extends PIDController {

	private static final double timeChange = .05; //Seconds per calculate call
	private static final boolean enabled = false;

	/*
	 true = moving
	 false = stopped
	 */
	private boolean state;

	private double destination;
	private double maxV;
	private double accel;

	private double lastDist, lastV;

	private double totalDist;

	private boolean firstCalculate, negative;

	private double decelStartDist;

	public CustomPIDController(double Kp, double Ki, double Kd, double Kf, PIDSource source, PIDOutput output, double period) {
		super(Kp, Ki, Kd, Kf, source, output, period);
		state = false;
	}

	public CustomPIDController(double Kp, double Ki, double Kd, PIDSource source, PIDOutput output, double period) {
		this(Kp, Ki, Kd, 0.0, source, output, period);
	}

	public CustomPIDController(double Kp, double Ki, double Kd, PIDSource source, PIDOutput output) {
		this(Kp, Ki, Kd, source, output, kDefaultPeriod);
	}

	public CustomPIDController(double Kp, double Ki, double Kd, double Kf, PIDSource source, PIDOutput output) {
		this(Kp, Ki, Kd, Kf, source, output, kDefaultPeriod);
	}

	public void setSetpoint(double destination, double maxV, double accel) {
		if (enabled) {
			this.negative = (destination < 0);
			this.destination = Math.abs(destination);
			this.maxV = maxV;
			this.accel = accel;
			this.firstCalculate = true;
			this.calculate();
		} else {
			this.setSetpoint(destination);
		}
	}

	public void calculate() {
		if (enabled) {
			if (firstCalculate) {
				double v = accel * timeChange;
				double s = (v * v) / (2 * accel);

				if (2 * s > destination) {
					decelStartDist = destination - s;
				} else {
					decelStartDist = destination / 2;
				}

				firstCalculate = false;
				lastDist = 0;
				lastV = 0;
				totalDist = 0;
				state = true;
			}

			if (state) {//Moving
				double newV = lastV + (accel * timeChange);
				newV = newV > maxV ? maxV : newV;
				if (totalDist >= destination) {// Reached Destination
					state = false;
					super.setSetpoint((negative ? -destination : destination));
					System.out.println("done!");
				} else {
					if (totalDist >= decelStartDist)//Decel
					{
						newV = lastV - (accel * timeChange);
					}
					lastV = newV;
					totalDist += lastDist = lastDist + newV * timeChange;
					super.setSetpoint(negative ? -totalDist : totalDist);
					System.out.println("Velocity: " + newV);
					System.out.println("Current Dest: " + lastDist);
					System.out.println("Total Dist: " + totalDist);
					System.out.println("****");
				}
			}
		}

		super.calculate();
	}

}
