package frc.team2856;

import edu.wpi.first.wpilibj.CounterBase.EncodingType;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Jaguar;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SpeedController;

/**
 *
 * @author jeschneider
 */
public class DriveTrain {
    private SpeedController leftBackJag, rightBackJag, leftFrontJag, rightFrontJag;
    public boolean arcade;
    private RobotDrive rd;

    /************************PID STUFF********************************/
    private final double DEFAULT_OUTPUT_RANGE_PID = .5, DEFAULT_OUTPUT_RANGE = 1.0;

    private CustomPIDController pidLeft, pidRight;

    private Encoder encodeLeft, encodeRight;

    private double P = 1.0,
                               I = 0.0, 
                               D = 0.0;
    private double outputRange, outputRangePID;

    public DriveTrain() {leftBackJag = new ReverseJaguar(RobotMap.DRIVE_JAGUAR_SLOT, RobotMap.LEFT_BACK_DRIVE_JAGUAR_CHANNEL);//, RobotMap.DRIVE_ACCEL_LIMIT);
        leftFrontJag = new ReverseJaguar(RobotMap.DRIVE_JAGUAR_SLOT, RobotMap.LEFT_FRONT_DRIVE_JAGUAR_CHANNEL);//, RobotMap.DRIVE_ACCEL_LIMIT);
        rightBackJag = new Jaguar(RobotMap.DRIVE_JAGUAR_SLOT, RobotMap.RIGHT_BACK_DRIVE_JAGUAR_CHANNEL);//, RobotMap.DRIVE_ACCEL_LIMIT);
        rightFrontJag = new Jaguar(RobotMap.DRIVE_JAGUAR_SLOT, RobotMap.RIGHT_FRONT_DRIVE_JAGUAR_CHANNEL);//, RobotMap.DRIVE_ACCEL_LIMIT);
        arcade = false;
        rd = new RobotDrive(leftFrontJag, leftBackJag, rightFrontJag, rightBackJag);
        
        /**********************PID STUFF********************/
        
        outputRange = DEFAULT_OUTPUT_RANGE;
        outputRangePID = DEFAULT_OUTPUT_RANGE_PID;

        encodeLeft = new Encoder(RobotMap.LEFT_ENCODER_CHANNEL_A, RobotMap.LEFT_ENCODER_CHANNEL_B, RobotMap.LEFT_ENCODER_DIRECTION, EncodingType.k4X);
        encodeRight = new Encoder(RobotMap.RIGHT_ENCODER_CHANNEL_A, RobotMap.RIGHT_ENCODER_CHANNEL_B, RobotMap.RIGHT_ENCODER_DIRECTION, EncodingType.k4X);

        //encodeLeft.setPIDSourceParameter(Encoder.PIDSourceParameter.kDistance);
        //encodeRight.setPIDSourceParameter(Encoder.PIDSourceParameter.kDistance);

        encodeLeft.setDistancePerPulse(0.0357541899441340782122905027933/12);
        encodeRight.setDistancePerPulse(0.0357541899441340782122905027933/12);
        
        encodeLeft.start();
        encodeRight.start();

        //pidLeft = new CustomPIDController(P, I, D, encodeLeft, leftBackJag, PIDController.kDefaultPeriod);
        //pidRight = new CustomPIDController(P, I, D, encodeLeft, rightBackJag, PIDController.kDefaultPeriod);

        //pidLeft.setContinuous();
        //pidRight.setContinuous();

        //pidLeft.setOutputRange(-outputRangePID, outputRangePID);
        //pidRight.setOutputRange(-outputRangePID, outputRangePID);
    }

    public void move(double left, double right) {
        //right = MotorLimiter.getLimitedSpeed(right, MotorLimiter.DRIVE_RIGHT);
        //left = MotorLimiter.getLimitedSpeed(left, MotorLimiter.DRIVE_LEFT);
        if(arcade)
            rd.arcadeDrive(left, right);
        else
            rd.tankDrive(left, right);
    }
        
    public void update() {
        /*((LimitedJaguar)leftBackJag).update();
        ((LimitedJaguar)leftFrontJag).update();
        ((LimitedJaguar)rightBackJag).update();
        ((LimitedJaguar)rightFrontJag).update();*/
    }
	
    public void resetEncoders(){
        encodeLeft.reset();
        encodeRight.reset();
        //encodeLeft.start();
        //encodeRight.start();
    }
    
    public double getLeftEncoderDistance() {
        return encodeLeft.getDistance();
    }
    
    public double getRightEncoderDistance() {
        return encodeRight.getDistance();
    }

    public void stop() {
//        pidLeft.disable();
  //      pidRight.disable();

    //    rd.stopMotor();
      //  rd.setSafetyEnabled(true);
    }

    /*****************************************************************/
    /************************START PID CODE***************************/
    /*****************************************************************/
/*    
	public void moveDist(double leftDist, double rightDist, double maxV, double accel) {
		rd.setSafetyEnabled(false);
		encodeLeft.reset();
		encodeRight.reset();
		pidRight.reset();
		pidLeft.reset();
		pidRight.enable();
		pidLeft.enable();
		pidLeft.setSetpoint(leftDist, maxV, accel);
		pidRight.setSetpoint(rightDist, maxV, accel);
	}
	
	public double[] getLeftPID(){
		return new double[] {pidLeft.getP(), pidLeft.getI(), pidLeft.getD()};
	}
	
	public double[] getRightPID(){
		return new double[] {pidRight.getP(), pidRight.getI(), pidRight.getD()};
	}
	
	public double getOutputRange(){
		return outputRange;
	}
	
	public double getOutputRangePID(){
		return outputRangePID;
	}
	
	public void incrementP(){
        double p,i,d;
        p = pidLeft.getP();
        i = pidLeft.getI();
        d = pidLeft.getD();
        p += 0.1;
        pidLeft.setPID(p, i, d);
        p = pidRight.getP();
        i = pidRight.getI();
        d = pidRight.getD();
        p += 0.1;
        pidRight.setPID(p, i, d);
    }

    public void incrementI(){
        double p,i,d;
        p = pidLeft.getP();
        i = pidLeft.getI();
        d = pidLeft.getD();
        i += 0.01;
        pidLeft.setPID(p, i, d);
        p = pidRight.getP();
        i = pidRight.getI();
        d = pidRight.getD();
        i += 0.01;
        pidRight.setPID(p, i, d);
    }

    public void incrementD(){
        double p,i,d;
        p = pidLeft.getP();
        i = pidLeft.getI();
        d = pidLeft.getD();
        d += 0.1;
        pidLeft.setPID(p, i, d);
        p = pidRight.getP();
        i = pidRight.getI();
        d = pidRight.getD();
        d += 0.1;
        pidRight.setPID(p, i, d);
    }
	
	public void decrementP(){
        double p,i,d;
        p = pidLeft.getP();
        i = pidLeft.getI();
        d = pidLeft.getD();
        p -= 0.1;
        pidLeft.setPID(p, i, d);
        p = pidRight.getP();
        i = pidRight.getI();
        d = pidRight.getD();
        p -= 0.1;
        pidRight.setPID(p, i, d);
    }

    public void decrementI(){
        double p,i,d;
        p = pidLeft.getP();
        i = pidLeft.getI();
        d = pidLeft.getD();
        i -= 0.01;
        pidLeft.setPID(p, i, d);
        p = pidRight.getP();
        i = pidRight.getI();
        d = pidRight.getD();
        i -= 0.01;
        pidRight.setPID(p, i, d);
    }

    public void decrementD(){
        double p,i,d;
        p = pidLeft.getP();
        i = pidLeft.getI();
        d = pidLeft.getD();
        d -= 0.1;
        pidLeft.setPID(p, i, d);
        p = pidRight.getP();
        i = pidRight.getI();
        d = pidRight.getD();
        d -= 0.1;
        pidRight.setPID(p, i, d);
    }

	public void setOutputRangePID(double output) {
		outputRangePID = output;
		pidLeft.setOutputRange(-outputRangePID, outputRangePID);
		pidRight.setOutputRange(-outputRangePID, outputRangePID);
	}
	
	public void setOutputRange(double output) {
		outputRange = output;
	}
	
	public void autoInit(){
		moveDist(500, 500, 2, .25);
	}
*/        
	/****************************************************************/
        /************************END PID CODE****************************/
        /*************START ENCAPSULATED DRIVETRAIN CLASSES**************/
        /****************************************************************/
        
	public static class ReverseJaguar extends Jaguar {
            public ReverseJaguar(int module, int port) {
                super(module, port);
            }
            //Calls the super set method and gives it the negated speed.
            public void set(double speed) {
                super.set(-speed);
            }
	}
        
	public static class ReverseLimitedJaguar extends LimitedJaguar {
            public ReverseLimitedJaguar(int slot, int channel, double a) {
                super(slot, channel, a);
            }
    
            public ReverseLimitedJaguar(int channel, double a) {
                super(1, channel, a);
            }
            //Calls the super set method and gives it the negated speed
            public void set(double speed) {
                super.set(-speed);
            }
        }
}