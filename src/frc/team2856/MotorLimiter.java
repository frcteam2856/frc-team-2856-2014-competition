/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package frc.team2856;

/**
 *
 * @author mitchell
 */
public class MotorLimiter {
	
	//Motors
	public static final int DRIVE_LEFT = 0, DRIVE_RIGHT = 1; 
	
	private static final int NUMBER_OF_MOTORS = 2;
	private static double[] motorSpeeds;
	
        //the periodic functions are called with about 50 Htz; should this be lower?
	public static double SPEED_THRESHHOLD = .2;
	
	public static void init(){
		motorSpeeds = new double[NUMBER_OF_MOTORS];
		for(int i = 0; i < NUMBER_OF_MOTORS; i++)
			motorSpeeds[i] = 0.0f;
	}
	
	public static double getLimitedSpeed(double speed, int motor){
		double oldSpeed = motorSpeeds[motor];
		
		if(Math.abs(speed - oldSpeed) >= SPEED_THRESHHOLD){
			if(speed - oldSpeed > 0){
				return motorSpeeds[motor] = speed + SPEED_THRESHHOLD;
			}else if(speed - oldSpeed < 0){
				return motorSpeeds[motor] = speed - SPEED_THRESHHOLD;
			}else{
				return speed;
			}
		}else{
			return motorSpeeds[motor] = speed;
		}
	}
	
}
