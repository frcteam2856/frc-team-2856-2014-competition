/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc.team2856;

/**
 *
 * @author jeschneider
 */
public class RobotMap {
    public static final String[] NETWORK_TABLE_VALUES = {"PIVOT_ANGLE"};
    public static final String IP_ADDRESS= "10.28.56.6",
            TABLE_NAME="BLAH";
    public static final int LEFT_JOYSTICK_PORT = 1,
							RIGHT_JOYSTICK_PORT = 2;
    public static final int XBOX_CONTROLER_PORT = 3;
    public static final int DRIVE_JAGUAR_SLOT = 1,
							LEFT_BACK_DRIVE_JAGUAR_CHANNEL = 2,
							RIGHT_BACK_DRIVE_JAGUAR_CHANNEL = 4,
							LEFT_FRONT_DRIVE_JAGUAR_CHANNEL = 1,
							RIGHT_FRONT_DRIVE_JAGUAR_CHANNEL = 3;
    public static final int CLAW_MOTOR_SLOT = 1,
							LEFT_CLAW_MOTOR_CHANNEL = 5,
							RIGHT_CLAW_MOTOR_CHANNEL = 7,
							LEFT_CLAW_PIVOT_CHANNEL = 6,
							RIGHT_CLAW_PIVOT_CHANNEL = 8;
    public static final int LAUNCHER_EXTEND_CHANNEL = 1,
							LAUNCHER_RETRACT_CHANNEL = 2,
							SAIL_EXTEND_CHANNEL = 3,
							SAIL_RETRACT_CHANNEL = 4,
							SOLENOID_CHANNEL_5 = 5,
							SOLENOID_CHANNEL_6 = 6;
    public static final int LEFT_ENCODER_CHANNEL_A = 3,
							LEFT_ENCODER_CHANNEL_B = 4,
							RIGHT_ENCODER_CHANNEL_A = 2,
							RIGHT_ENCODER_CHANNEL_B = 1;
    
    public static final double MAX_SERVO_ANGLE = 90, MIN_SERVO_ANGLE = 0, SERVO_ANGLE_CHANGE = 5;
    public static final int SERVO_SLOT = 1, SERVO_CHANNEL = 9;
    
    public static final boolean LEFT_ENCODER_DIRECTION = false,
								RIGHT_ENCODER_DIRECTION = false;
    public static final int CLAW_LP_SLOT = 1,
							LEFT_CLAW_LP_CHANNEL = 1,
							LEFT_CLAW_PIVOT_LP_CHANNEL = 2,
							RIGHT_CLAW_LP_CHANNEL = 3,
							RIGHT_CLAW_PIVOT_LP_CHANNEL = 4;
    public static final int COMPRESSOR_SLOT = 1,
	    COMPRESSOR_CHANNEL = 1,
	    PRESSURE_SWITCH_SLOT = 1,
	    PRESSURE_SWITCH_CHANNEL = 14;
    public static double DRIVE_ACCEL_LIMIT = 1.0;
    public static double CLAW_ARM_MAX_EFFORT = 0.1;
    public static double CLAW_PIV_MAX_EFFORT = 0.1;
}
