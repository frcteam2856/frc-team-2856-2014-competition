/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc.team2856;

import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 *
 * @author Morgan
 */
public abstract class InputManager {

    protected DriveTrain drive;
    protected Claw claw;
    protected Launcher launcher;

    public abstract void update();

    protected static class CustomButton {

        private boolean lastValue;
        private JoystickButton jb;

        public CustomButton(JoystickButton jb) {
            this.jb = jb;
            lastValue = jb.get();
        }

        public boolean hasChanged() {
            return lastValue != jb.get();
        }

        public boolean get() {
            return hasChanged() && (lastValue = jb.get());
        }
    }
}
