/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc.team2856;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 *
 * @author Morgan
 */
public class XBoxInputManager extends InputManager {

    private Joystick cont;
    private CustomButton[] buttons;

    public XBoxInputManager(DriveTrain dt) {
        drive = dt;
        cont = new Joystick(RobotMap.XBOX_CONTROLER_PORT);

        // FIXME
        int numbuttons = 9;
        buttons = new CustomButton[numbuttons];

        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new CustomButton(new JoystickButton(cont, i));
        }
    }

    public void update() {
        drive.move(cont.getRawAxis(2), cont.getRawAxis(5));
    }
}
