/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc.team2856;

import edu.wpi.first.wpilibj.networktables.*;

/**
 *
 * @author jeschneider
 */
public class CommunicationManager {
    NetworkTable table;
    AerialAssistBot master;
    
    /*
     * rightVert
     * rightHoriz
     * leftVert
     * leftHoriz
     * launchOn
     * sailsOut
     */
    
    public CommunicationManager(AerialAssistBot m) {
        try {
            table = NetworkTable.getTable("SmartDashboard");
        }
        catch(Exception e) {
            System.out.println("Unable to access the NetworkTable");
            e.printStackTrace();
        }
        master = m;
    }
    
    public void update() {
        table.putNumber("Left Claw Horizontal", master.claw.getLeftVoltage());
        table.putNumber("Right Claw Horizontal", master.claw.getRightVoltage());
        table.putNumber("Left Claw Verticle", master.claw.getLeftPivotVoltage());
        table.putNumber("Right Claw Verticle", master.claw.getLeftPivotVoltage());
        table.putBoolean("Enforce Limits", master.claw.enforceLimits);
        table.putNumber("Left Encoder", master.drive.getLeftEncoderDistance());
        table.putNumber("Right Encoder", master.drive.getRightEncoderDistance());
        table.putNumber("Camera Angle", master.cameraServo.getAngle());
        /*double target = -1;
        if((target = table.getNumber("left")) != -1)
            master.claw.setLeftClawTarget((int)target);
        if((target = table.getNumber("right")) != -1)
            master.claw.setRightClawTarget((int)target);
        if((target = table.getNumber("leftPivot")) != -1)
            master.claw.setLeftPivotTarget((int)target);
        if((target = table.getNumber("rightPivot")) != -1)
            master.claw.setRightPivotTarget((int)target);*/
    }
    
    
    
    /*//if -1 is returned,then there is no entry in that index, if -2 is returned, then there is no entry under such valueKey.
    public double getValue(int index){
        String valueKey;
        double value;
        if(index<RobotMap.NETWORK_TABLE_VALUES.length && index>=0)
            valueKey= RobotMap.NETWORK_TABLE_VALUES[index];
        else 
            valueKey= "null";
        if(valueKey=="null")
            return -1;
        else{
            value = table.getDouble(valueKey, -2);
        }
        return value;
    } */
}