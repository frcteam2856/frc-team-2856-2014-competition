/*----------------------------------------------------------------------------*/
/* Copyright (c) FIRST 2008. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team2856;


import edu.wpi.first.wpilibj.*;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class AerialAssistBot extends IterativeRobot {
	
	DriveTrain drive;
	private JoyBoxInputManager im;
	public Claw claw;
	private Launcher launch;
        private Sail sail;
        CommunicationManager communicator;
	private Compressor compress;
        Servo cameraServo;
	
    /**
     * This function is run when the robot is first started up and should be
     * used for any initialization code.
     */
    public void robotInit() {
		drive = new DriveTrain();
		launch = new Launcher();
		claw = new Claw();
                communicator = new CommunicationManager(this);
                sail = new Sail();
		compress = new Compressor(RobotMap.PRESSURE_SWITCH_SLOT,
			RobotMap.PRESSURE_SWITCH_CHANNEL,
			RobotMap.COMPRESSOR_SLOT,
			RobotMap.COMPRESSOR_CHANNEL);
		compress.start();
                cameraServo = new Servo(RobotMap.SERVO_SLOT, RobotMap.SERVO_CHANNEL);
		im = new JoyBoxInputManager(drive, claw, launch, sail, cameraServo);
    }
	
    /**
     * This function is called periodically during autonomous
     */
    public void autonomousPeriodic() {
		
    }
	
	public void autonomousInit(){
		//drive.autoInit();
	}
	
	public void disabledInit(){
		drive.stop();
	}
	

    /**
     * This function is called periodically during operator control
     */
    public void teleopPeriodic() {
        im.update();
        claw.update();
        drive.update();
        communicator.update();
    }
	
    /**
     * This function is called periodically during test mode
     */
    public void testPeriodic() {
    
    }
    
}