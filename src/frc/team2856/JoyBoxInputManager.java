/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frc.team2856;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.Servo;

//https://docs.google.com/spreadsheet/ccc?key=0AlTdVbpSEWuTdF84S2NhRWlsclF2VkF2S25GeU1ueGc&usp=drive_web#gid=0
// xbox buttons http://answers.unity3d.com/questions/8094/what-are-all-the-joystick-buttons-for-an-xbox-360p.html
// xbox axis http://www.chiefdelphi.com/forums/showthread.php?t=83597 <<<<< scroll to post by jared341
/**
 *
 * @author mitchell
 */
public class JoyBoxInputManager extends InputManager {

    private final static int numRightButtons = 11, numLeftButtons = 11, numContButtons = 10;
    private Joystick rightJoystick, leftJoystick, contJoystick;
    private CustomButton[] leftButtons, rightButtons, contButtons;
    private DriveTrain drive;
    private Claw claw;
    private Launcher launch;
    private Sail sail;
    private Servo cameraServo;

    public JoyBoxInputManager(DriveTrain dt, Claw c, Launcher l, Sail s, Servo cs) {
	drive = dt;
	claw = c;
	launch = l;
        sail = s;
        cameraServo = cs;

	rightJoystick = new Joystick(RobotMap.RIGHT_JOYSTICK_PORT);
	leftJoystick = new Joystick(RobotMap.LEFT_JOYSTICK_PORT);
	contJoystick = new Joystick(RobotMap.XBOX_CONTROLER_PORT);
        System.out.println("Default Joystick Values");
        System.out.println("Left X: "+leftJoystick.getX());
        System.out.println("Left Y: "+leftJoystick.getY());
        System.out.println("Left Z: "+leftJoystick.getZ());
        System.out.println("Right X: "+rightJoystick.getX());
        System.out.println("Right Y: "+rightJoystick.getY());
        System.out.println("Right Z: "+rightJoystick.getZ());
        System.out.println("Xbox 1: "+contJoystick.getRawAxis(1));
        System.out.println("Xbox 2: "+contJoystick.getRawAxis(2));
        System.out.println("Xbox 3: "+contJoystick.getRawAxis(3));
        System.out.println("Xbox 4: "+contJoystick.getRawAxis(4));
        System.out.println("Xbox 5: "+contJoystick.getRawAxis(5));

	leftButtons = new CustomButton[numLeftButtons + 1];
	for (int i = 0; i < leftButtons.length; i++) {
	    leftButtons[i] = new CustomButton(new JoystickButton(leftJoystick, i));
	}

	rightButtons = new CustomButton[numLeftButtons + 1];
	for (int i = 0; i < rightButtons.length; i++) {
	    rightButtons[i] = new CustomButton(new JoystickButton(rightJoystick, i));
	}

	contButtons = new CustomButton[numLeftButtons + 1];
	for (int i = 0; i < contButtons.length; i++) {
	    contButtons[i] = new CustomButton(new JoystickButton(contJoystick, i));
	}
    }

    public void update() {

	if (rightButtons[1].get()) {
	    System.out.println("ardading");
	    drive.arcade = true;
	} else {
	    drive.arcade = false;
	}
	drive.move(leftJoystick.getY(), rightJoystick.getY());
        
        if(leftButtons[4].get())
            cameraServo.setAngle(Math.max(cameraServo.getAngle() - RobotMap.SERVO_ANGLE_CHANGE, RobotMap.MIN_SERVO_ANGLE));
        if(leftButtons[5].get())
            cameraServo.setAngle(Math.min(cameraServo.getAngle() + RobotMap.SERVO_ANGLE_CHANGE, RobotMap.MAX_SERVO_ANGLE));
        
        
	if (rightButtons[3].get()) {
	    drive.stop();
	}

	if (rightButtons[5].get()) {
	    drive.stop();
	    claw.stop();
	}
        
        if(contButtons[6].get()) {
            if(launch.getState())
                launch.retract();
            else
                launch.extend();
        }

        if(contButtons[5].get()) {
            if(sail.getState())
                sail.retract();
            else
                sail.extend();
        }
        
        //System.out.println("Left Joystick Y: "+leftJoystick.getY());
        //System.out.println("Right Joystick Y: "+rightJoystick.getY());
        //claw.pivotLeft(leftJoystick.getY());
       // claw.moveLeft(leftJoystick.getX());
        //claw.pivotRight(rightJoystick.getY());
        //claw.moveRight(rightJoystick.getX());
        
        //lefty
        if(Math.abs(contJoystick.getRawAxis(2)) < 0.4)
            claw.pivotLeft(0);
        else
            claw.pivotLeft(-1*contJoystick.getRawAxis(2));
        
        System.out.println("Left Arm Joystick Value: "+contJoystick.getRawAxis(1));
        
        //leftx
        if(Math.abs(contJoystick.getRawAxis(1)) < 0.4)
            claw.moveLeft(0);
        else
            claw.moveLeft(contJoystick.getRawAxis(1));
        
        
        //righty
        if(Math.abs(contJoystick.getRawAxis(5)) < 0.4)
            claw.pivotRight(0);
        else
            claw.pivotRight(-1*contJoystick.getRawAxis(5));
        
        //rightx
        if(Math.abs(contJoystick.getRawAxis(4)) < 0.4)
            claw.moveRight(0);
        else
            claw.moveRight(contJoystick.getRawAxis(4));
        
        /*if(contButtons[3].get())
            claw.enforceLimits = !claw.enforceLimits;
        */
        RobotMap.CLAW_ARM_MAX_EFFORT = (leftJoystick.getZ()+1)/5;
        RobotMap.CLAW_PIV_MAX_EFFORT = (rightJoystick.getZ()+1)/2;
        System.out.println("Max Arm Effort: "+RobotMap.CLAW_ARM_MAX_EFFORT);
        System.out.println("Max Piv Effort: "+RobotMap.CLAW_PIV_MAX_EFFORT);
        System.out.println();
    }
}
