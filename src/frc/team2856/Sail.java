/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package frc.team2856;

import edu.wpi.first.wpilibj.Solenoid;

/**
 *
 * @author mitchell
 */
public class Sail {
	private Solenoid extend, retract;
	public boolean extended;
	public Sail(){
		extend = new Solenoid(RobotMap.SAIL_EXTEND_CHANNEL);
		retract = new Solenoid(RobotMap.SAIL_RETRACT_CHANNEL);
	}
	public void extend() {
		retract.set(false);
		extend.set(true);
        extended = true;

    }

    /**
     * unfinished retracts sail piston
     */
    public void retract() {
		extend.set(false);
		retract.set(true);
        extended = false;

    }

    /**
     * unfinished returns extension state of sail piston probably best to
     * use a boolean value? boolean - true=extended false=retracted
     */
    public boolean getState() {
        return extended;
    }
}
