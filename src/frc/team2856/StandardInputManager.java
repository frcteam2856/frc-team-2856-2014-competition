package frc.team2856;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 *
 * @author jeschneider
 */
public class StandardInputManager extends InputManager {

    private Joystick leftJoystick, rightJoystick;
    private CustomButton[] leftButtons, rightButtons;
    private boolean lClawOpen, lClawClose, rClawOpen, rClawClose;

    public StandardInputManager(DriveTrain dt, Claw c, Launcher l) {
        drive = dt;
        claw = c;
        launcher = l;

        leftJoystick = new Joystick(RobotMap.LEFT_JOYSTICK_PORT);
        rightJoystick = new Joystick(RobotMap.RIGHT_JOYSTICK_PORT);

        leftButtons = new CustomButton[12];
        rightButtons = new CustomButton[12];

        for (int i = 0; i < 12; i++) {
            leftButtons[i] = new CustomButton(new JoystickButton(leftJoystick, i));
            rightButtons[i] = new CustomButton(new JoystickButton(rightJoystick, i));
        }

        lClawOpen = leftButtons[0].get();
        lClawClose = leftButtons[1].get();
        rClawOpen = rightButtons[0].get();
        rClawClose = rightButtons[1].get();
    }

    public void update() {
               if(rightButtons[1].get()) {
                   System.out.println("ardading");
                   drive.arcade = true;
               }
               else
                   drive.arcade = false;
		drive.move(leftJoystick.getY(), rightJoystick.getY());
		
		//if(leftButtons[10].hasChanged() && leftButtons[10].get())
		//	drive.resetEncoders();
		
		//claw.armLeft(leftJoystick.getY());
		//claw.armRight(rightJoystick.getY());
		
		//claw.pivLeft(leftJoystick.getX());
		//claw.pivRight(rightJoystick.getX());
    }

}
