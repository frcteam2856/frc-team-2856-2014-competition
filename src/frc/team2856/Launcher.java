/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frc.team2856;

import edu.wpi.first.wpilibj.Solenoid;

/**
 *
 * @author jeschneider
 */
public class Launcher {

    private boolean extended = true;
    //need to rename items once mechanical&electical FIO
    private Solenoid extend = new Solenoid(RobotMap.LAUNCHER_EXTEND_CHANNEL),
            retract = new Solenoid(RobotMap.LAUNCHER_RETRACT_CHANNEL);

    public Launcher() {
    }

    /**
     * unfinished extends launcher pistons
     */
    public void extend() {
		retract.set(false);
		extend.set(true);
        extended = true;

    }

    /**
     * unfinished retracts launcher pistons
     */
    public void retract() {
		extend.set(false);
		retract.set(true);
        extended = false;

    }

    /**
     * unfinished returns extension state of launcher pistons probably best to
     * use a boolean value? boolean - true=extended false=retracted
     */
    public boolean getState() {
        return extended;
    }
}
